package pl.edu.wi.cez.fingerPrint;

import pl.edu.wi.cez.fingerPrint.viewer.Viewer;

public class Main {
    public static void main(String[] args) {
        Viewer viewer = new Viewer();
        viewer.setVisible(true);
    }
}
